$pdf_mode = 1;
$preview_continuous_mode = 1;
$do_cd = 1;
$cleanup_mode = 1;
$pdflatex = "xelatex -src-specials -synctex=0 -interaction=nonstopmode %O %S";
$pdf_previewer = "start evince %O %S";
$pdf_update_method = 0;
$sleep_time = 1;
