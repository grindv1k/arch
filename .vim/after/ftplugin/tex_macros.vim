call IMAP(',.sec', '\section*{<++>}<++>', 'tex')
call IMAP(',.sub', '\subsection*{<++>}<++>', 'tex')
call IMAP(',.vec', '\vec{<++>}<++>', 'tex')
call IMAP(',.hat', '\hat{<++>}<++>', 'tex')
call IMAP(',.eq', "\\[\<return><++>\<return>\\]\<return><++>", 'tex')
call IMAP(',.ut', '\ut{<++>}<++>', 'tex')
call IMAP(',.uu', '\uu{<++>}<++>', 'tex')
call IMAP(',.tilde', '\tilde{<++>}<++>', 'tex')
call IMAP(',.dot', '\dot{<++>}<++>', 'tex')
call IMAP(',.cdot', '\cdot <++>', 'tex')
call IMAP(',.pow', '^{<++>}<++>', 'tex')
call IMAP(',.ieq', '$<++>$<++>', 'tex')
call IMAP(',.fr', '\frac{<++>}{<++>}<++>', 'tex')
call IMAP(',.int', '\int_{<++>}^{<++>}<++>', 'tex')
call IMAP(',.oint', '\oint_{<++>}^{<++>}<++>', 'tex')
call IMAP(',.pic', '\pic{<++>}{<++>}{<++>}<++>', 'tex')
call IMAP(',.align', "\\begin{align*}\<return>&<++>\<return>\\end{align*}<++>", 'tex')
call IMAP(',.item', "\\begin{itemize}\<return>\\item<++>\<return>\\end{itemize}<++>", 'tex')
call IMAP(',.text', '\text{<++>}<++>', 'tex')
call IMAP(',.mbf', '\mathbf{<++>}<++>', 'tex')
call IMAP(',.diff', '\textrm{d}<++>', 'tex')
call IMAP(',.mit', '\mathit{<++>}<++>', 'tex')
call IMAP(',.big[', '\Big[<++>\Big]<++>' ,'tex')
call IMAP(',.big(', '\Big(<++>\Big)<++>' ,'tex')
call IMAP(',.big|', '\Big|<++>\Big|<++>', 'tex')
call IMAP(',.oppgbox', "\\oppgbox{\<return><++>\<return>}\\\\<++>", 'tex')
call IMAP(',.sqrt', '\sqrt{<++>}<++>', 'tex')
call IMAP(',.sum', '\sum_{<++>}^{<++>}<++>', 'tex')
call IMAP(',.lim', '\lim_{<++> \rightarrow <++>\infty}<++>', 'tex')
call IMAP('`o', '\omega<++>', 'tex')
call IMAP('`O', '\Omega<++>', 'tex')
