#!/bin/bash
# Input arguments:
figures=$1

echo "Enter name of course (fluid | regtek | objprog | elmag)"
read coursename

until [ "$coursename" == "fluid" ] ||
    [ "$coursename" == "regtek" ] ||
    [ "$coursename" == "objprog" ] || 
[ "$coursename" == "elmag" ] 
do
    echo "Enter name of course (fluid | regtek | objprog | elmag)"
    read coursename
done

echo "Enter number of exercise (XX) ..."

read exerciseNumber

varLength=${#exerciseNumber}
until [ $varLength == 2 ] && [ $exerciseNumber -eq $exerciseNumber ] 2>/dev/null
do
    echo "Try again ..."
    echo "Input number (XX): "
    read exerciseNumber
    varLength=${#exerciseNumber}
done

dirname="ov$exerciseNumber"
proj=/home/torstgri/NTNU/"$coursename"/"$dirname"



if [[ ! -d "$proj" && ! -L "$proj" ]] ; then
    # Create directory for new oving.
    mkdir -p "$proj"
    mkdir -p "$proj/aux"
    echo "Directory created ..."

    # Copy files into new oving. Should have an extra check.
    cp -i /home/torstgri/NTNU/template/main.tex                 $proj   
    cp -i /home/torstgri/NTNU/template/exerciseSettings.tex     $proj
    cp -i /home/torstgri/NTNU/template/exerciseBody.tex         $proj

    echo "Files created ..."

    # Ask for figures here
    if [ $1 == "--fig" ] 2>/dev/null ; then
        COUNTER=1
        echo "Do you want to add figures to project folder? (y/n)"
        read answer
        if [ "$answer" == "y" ] ; then
            mkdir -p "$proj/figures"
            scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
            echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
        fi

        until [ "$answer" == "n" ]
        do
            # Take screenshot of area, then add to project folder.
            # Also include it in latex.
            echo "One more?"
            read answer
            if [ "$answer" == "y" ] ; then
                let COUNTER=COUNTER+1 
                scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
                echo "\\section*{Oppgave $COUNTER}" >>  $proj/exerciseBody.tex
                echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
            fi
        done
    fi

    # Add things that should be automated
    exerciseNumber=$(echo $dirname | cut -d'v' -f 2)
    echo "\\newcommand{\\whichExercise}{ØVING $exerciseNumber }" >> $proj/exerciseSettings.tex
    echo "\\toggletrue{$coursename}" >> $proj/exerciseSettings.tex
    vim $proj/exerciseSettings.tex
    urxvt -e latexmk -quiet -jobname="$coursename$exerciseNumber" $proj/main.tex &
    vim $proj/exerciseBody.tex
else
    # Ask for figures here too
    if [ $1 == "--fig" ] 2>/dev/null ; then
        COUNTER=1
        echo "Do you want to add figures to project folder? (y/n)"
        read answer
        if [ "$answer" == "y" ] ; then
            mkdir -p "$proj/figures"
            scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
            echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
        fi

        until [ "$answer" == "n" ]
        do
            # Take screenshot of area, then add to project folder.
            # Also include it in latex.
            echo "One more?"
            read answer
            if [ "$answer" == "y" ] ; then
                let COUNTER=COUNTER+1 
                scrot -s "fig$COUNTER.png" -e "mv fig$COUNTER.png $proj/figures"
                echo "\\section*{Oppgave $COUNTER}" >>  $proj/exerciseBody.tex
                echo "\\pic{figures/fig$COUNTER}{0.5}{}" >> $proj/exerciseBody.tex
            fi
        done
    fi
    echo "The directory already exists. Opening files in vim..."
    urxvt -e latexmk -quiet -jobname="$coursename$exerciseNumber" $proj/main.tex &
    vim $proj/exerciseBody.tex
fi
