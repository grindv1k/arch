# Path to your oh-my-zsh installation.
export ZSH=/home/torstgri/.oh-my-zsh
# export SHELL=/usr/bin/zsh
export MATLAB_JAVA=/usr/lib/jvm/java-8-openjdk/jre

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="afowler"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(gitfast)

# User configuration

export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/torstgri/bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh
export MANPAGER="/bin/sh -c \"col -b | vim -c 'set ft=man ts=8 nolist nonu noma nomod ' -\""

export VISUAL="vim"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

alias cl=clear
alias spac="sudo pacman -S"
alias progg="vim *.h *.cpp Makefile"
alias shutdown="sudo shutdown now"
alias scrn="scrot -s"
alias zshrc="vim ~/.zshrc"
alias vimrc="vim ~/.vimrc"
alias vimmacro="vim ~/.vim/after/ftplugin/tex_macros.vim"
alias xinitrc="vim ~/.xinitrc"
alias i3configrc="vim ~/.config/i3/config"
alias i3statusrc="vim ~/.i3status.conf"
alias xresourcesrc="vim ~/.Xresources" 
alias xbindkeysrc="vim ~/.xbindkeysrc"
alias susp="systemctl suspend"
alias colors="cd ~/.config/base16-shell && ./colortest && cd"
alias gløshævven="work.sh"
