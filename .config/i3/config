# i3 config file (v4)

font pango:FontAwesome, Source Code Pro 12
# font pango: Hermit 11
# font pango:FontAwesome, Terminus 13
# font pango: Bookman 12
# font pango: InputMonoCondensed Black, FontAwesome 11
# font pango: BigBlue TerminalPlus 9, FontAwesome 
# font pango:FontAwesome, BigBlue TerminalPlus 10

# Hide them the way i3-gaps tells you to. 
for_window [class="^.*"] border pixel 4

# Gaps settings
#smart_gaps on
gaps inner 25
# gaps outer 1

set $up k
set $down j
set $left h
set $right l 

# use Mouse+Mod1 to drag floating windows to their wanted position
floating_modifier Mod1

#### Bindings for programs
bindsym Mod1+Ctrl+p exec matlab -desktop
bindsym Mod1+Ctrl+c exec chromium
bindsym Mod1+Ctrl+s exec spotify -name spotify -e spotify
bindsym Mod1+Ctrl+g exec steam 
bindsym Mod1+Ctrl+e exec evince
bindsym Mod1+Ctrl+m exec maple -x
bindsym Mod1+Ctrl+j exec java -jar /home/torstein/Applets/circuit.jar
bindsym Mod1+Ctrl+v exec urxvt -e vim -c MRU
bindsym Mod1+Ctrl+t exec $HOME/bin/retro-term.sh
bindsym Mod1+Ctrl+a exec anki
bindsym Mod1+Ctrl+f exec firefox-beta

bindsym Mod1+Ctrl+Shift+s exec systemctl suspend
###
# Take a screenshot 
bindsym --release Shift+Print exec scrot -s '%m-%d_$wx$h.png' -e 'mv $f ~/Dropbox/Arch/'

# start a terminal
bindsym Mod1+Return exec i3-sensible-terminal
bindsym Mod1+p exec xterm

# kill focused window
bindsym Mod1+Shift+q kill


# change focus
bindsym Mod1+$left focus left
bindsym Mod1+$down focus down
bindsym Mod1+$up focus up
bindsym Mod1+$right focus right

# move focused window
bindsym Mod1+Shift+$left move left
bindsym Mod1+Shift+$down move down
bindsym Mod1+Shift+$up move up
bindsym Mod1+Shift+$right move right

# split in horizontal orientation
bindsym Mod1+b split h

# split in vertical orientation
bindsym Mod1+v split v

# enter fullscreen mode for the focused container
bindsym Mod1+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym Mod1+s layout stacking
bindsym Mod1+w layout tabbed
bindsym Mod1+e layout toggle split

# toggle tiling / floating
bindsym Mod1+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym Mod1+space focus mode_toggle

bindsym Mod1+ctrl+Right workspace next
bindsym Mod1+ctrl+Left workspace prev

# move the currently focused window to the scratchpad
bindsym Mod1+Shift+minus move scratchpad

# Move current workspace to next monitor
bindsym Mod1+Shift+m move workspace to output right

# Show the next scratchpad window or hide the focused scratchpad window.
# If there are multiple scratchpad windows, this command cycles through them.
bindsym Mod1+minus scratchpad show

# fat circle
# three short lines
#• utf8 general punctuation dot
set $wspace1 1•
set $wspace2 2•
set $wspace3 3•
set $wspace4 4•
set $wspace5 5•
set $wspace6 6•
set $wspace7 7•
set $wspace8 8•
set $wspace9 9•
set $wspace10 10•

# switch to workspace
bindsym Mod1+1 workspace $wspace1
bindsym Mod1+2 workspace $wspace2
bindsym Mod1+3 workspace $wspace3
bindsym Mod1+4 workspace $wspace4
bindsym Mod1+5 workspace $wspace5
bindsym Mod1+6 workspace $wspace6
bindsym Mod1+7 workspace $wspace7
bindsym Mod1+8 workspace $wspace8
bindsym Mod1+9 workspace $wspace9
bindsym Mod1+0 workspace $wspace10

# move focused container to workspace
bindsym Mod1+Shift+1 move container to workspace $wspace1
bindsym Mod1+Shift+2 move container to workspace $wspace2
bindsym Mod1+Shift+3 move container to workspace $wspace3
bindsym Mod1+Shift+4 move container to workspace $wspace4
bindsym Mod1+Shift+5 move container to workspace $wspace5
bindsym Mod1+Shift+6 move container to workspace $wspace6
bindsym Mod1+Shift+7 move container to workspace $wspace7
bindsym Mod1+Shift+8 move container to workspace $wspace8
bindsym Mod1+Shift+9 move container to workspace $wspace9
bindsym Mod1+Shift+0 move container to workspace $wspace10

# Force certain application to cerctain worskspaces
# Use xprop to find class names.
assign [class="^Evince$"] $wspace4
for_window [class="^Evince$"] focus
# Maple
assign [class="^java-lang-Thread$"] $wspace10
for_window [class="^java-lang-Thread$"] focus
# Anki
assign [class="^Anki$"] $wspace10
for_window [class="^Anki$"] focus



# reload the configuration file
bindsym Mod1+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym Mod1+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym Mod1+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode
        bindsym $left       resize shrink width 10 px or 10 ppt
        bindsym $down       resize grow height 10 px or 10 ppt
        bindsym $up         resize shrink height 10 px or 10 ppt
        bindsym $right      resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left        resize shrink width 10 px or 10 ppt
        bindsym Down        resize grow height 10 px or 10 ppt
        bindsym Up          resize shrink height 10 px or 10 ppt
        bindsym Right       resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym Mod1+r mode "resize"

#### Set colors
# set $base00 #181818
# set $base01 #282828
# set $base02 #383838
# set $base03 #585858
# set $base04 #b8b8b8
# set $base05 #d8d8d8
# set $base06 #e8e8e8
# set $base07 #f8f8f8
# set $base08 #ab4642
# set $base09 #dc9656
# set $base0A #f7ca88
# set $base0B #a1b56c
# set $base0C #86c1b9
# set $base0D #7cafc2
# set $base0E #ba8baf
# set $base0F #a16946

# start dmenu
# bindsym Mod1+d exec dmenu_run -h 40 -fn 'Hermit-10' -y 512 -dim 0.7
# start rofi
bindsym Mod1+d exec rofi -show run -font "InputMonoCondensed Black 12" -fg $fg0 -bg $bg

# https://github.com/morhetz/gruvbox
## GRUVBOX DARK
set $fg #ebdbb2
set $fg0 #fbf1c7
set $fg1 #ebdbb2k
set $fg2 #d5c4a1
set $fg3 #bdae93
set $fg4  #a89984

# standard:
set $bg #282828
# Black + DarkGrey
set $bg      #282828
set $gray  #928374
# DarkRed + Red
set $red_dark #cc241d
set $red #fb4934
set $red_dark_gl #9d0006
# DarkGreen + Green
set $green_dark #98971a
set $green #b8bb26
# DarkYellow + Yellow
set $yellow_dark #d79921
set $yellow  #fabd2f
# DarkBlue + Blue
set $blue_dark  #458588
set $blue_dark_gl  #076678
set $blue #83a598
# DarkMagenta + Magenta
set $purple_dark #b16286
set $purple #d3869b
# DarkCyan + Cyan
set $aqua_dark #689d6a
set $aqua_dark_gl #427b58
set $aqua #8ec07c

# set some nice colors      border          background          text        indicator
client.focused              $fg0     $fg0     $bg   $fg0
client.unfocused            $fg0     $fg0     $fg2  $fg0
client.focused_inactive     $fg0     $fg3     $fg0  $fg0

# old bar: status_command i3status
bar {
    status_command SCRIPT_DIR=~/.config/i3/blocklets i3blocks
    tray_output primary
	position top
	separator_symbol " "
    
    colors {
		background 			$fg0
        statusline          $bg
        active_workspace    $fg0 	$fg2 $fg0
        focused_workspace   $fg0    $bg $fg0
        inactive_workspace  $fg0 	$fg2 $fg0
    }
}
