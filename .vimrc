set nocompatible    " Vundle req
filetype off        " Vundle req
filetype plugin indent on " Vundle req
let g:easytags_include_members = 1
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin() 
Plugin 'VundleVim/Vundle.vim' " Vundle req
Plugin 'vim-latex/vim-latex'
Plugin 'scrooloose/nerdcommenter'
" Plugin 'Yggdroot/indentLine'
Plugin 'PotatoesMaster/i3-vim-syntax'
Plugin 'ctrlpvim/ctrlp.vim'
" NOTE: Completion uses youcompleteme from AUR
Plugin 'jeetsukumaran/vim-buffergator'
Plugin 'morhetz/gruvbox'
Plugin 'easymotion/vim-easymotion'
Plugin 'vim-scripts/taglist.vim'
Plugin 'xolox/vim-misc' " Required for easytags
Plugin 'xolox/vim-easytags'
Plugin 'othree/html5-syntax.vim'
Plugin 'mattn/emmet-vim'

call vundle#end()

" Now save, quit, open vim, and run :PluginInstall

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" COLORS, VISUAL
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
syntax enable 
colorscheme gruvbox

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" LET & SET
" Latex suite
let Tex_FoldedSections=""
let Tex_FoldedEnvironments=""
let Tex_FoldedMisc=""
" mattn/emmet-vim
let g:user_emmet_install_global = 0
autocmd FileType html,css EmmetInstall
" vim-scripts/taglist.vim
let Tlist_Process_File_Always = 1
let Tlist_Exit_OnlyWindow = 1
let Tlist_Use_Right_Window = 1
let Tlist_WinWidth = 40
let Tlist_Compact_Format = 1
let Tlist_Enable_Fold_Column = 0
let Tlist_Display_Prototype = 0
" Youcompleteme (AUR)
let g:ycm_global_ycm_extra_conf = '/home/torstgri/NTNU/.ycm_extra_conf.py' " For cpp completion error thing YCM
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
" Yggdroot/indentline
let g:indentLine_char = '|' "Shows indentation
let g:indentLine_faster = 1
" Tex related
let g:tex_flavor="latex" " Lets empty .tex files avoid being set as plaintex
let g:Tex_DefaultTargetFormat = 'pdf' " Settings for latex-suite
" Misc
let mapleader = "\<space>"
let $LANG='en' 

set conceallevel=0
set background=dark
set hidden
set tags=./tags;/
set so=12 " Set 7 lines to the cursor - when moving vertically using j/k
set number " Shows line numbers
set relativenumber
set langmenu=en
set wildmenu " 
set ruler "Always show current position
set cmdheight=2 " Height of the command bar
set expandtab "Use spaces instead of tabs
set smarttab " ?
set shiftwidth=4 "1 tab == 4 spaces
set tabstop=4
set lbr "Linebreak on 500 characters
set tw=500
set ai "Auto indent
set si "Smart indent
set nowrap "Wrap lines
set laststatus=2 
set ignorecase "Ignore case when searching
set smartcase "When searching try to be smart about cases 
set hlsearch "Highlight search results
set incsearch "Makes search act like search in modern browsers
set showmatch "Show matching brackets
set mat=2 "How many tenths of a second to blink when matching brackets
set backupdir=~/.vim/backup//
set directory=~/.vim/swap//
set undodir=~/.vim/undo//
set history=500 " Sets how many lines of history VIM has to remember
set autoread " Set to auto read when a file is changed from the outside
set wrapmargin=2 " wrap line n characters from right border
set grepprg=grep\ nH\ $* " No confuse Latex-Suite.

highlight MyTagListFileName ctermfg=red
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"""" MAPPINGS, COMMANDS, AUTOCOMMANDS
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Paste in insert mode
" inoremap <leader>v <Esc>"*pa
" Paste in normal mode
nnoremap <leader>v "*pa<Esc>
" Select all
nmap <leader>a ggVG
" Toggle taglist
nnoremap <silent> <leader>tl :TlistToggle<cr>
" YOUCOMPLETEME
nmap <leader>f :YcmCompleter FixIt<cr>
" For jumping between .h and .cpp files 
nmap <leader>gt :YcmCompleter GoTo<cr>
" Jump to 2nd tag match, for jumping between declaration / definition
nmap <leader>d 2<c-]>
" Fast saving
nmap <leader>w :w!<cr>
nmap <leader>q :q<cr>
nmap <leader>p :CtrlPMRU<cr>
" Jump thing latex suite
imap jk <Plug>IMAP_JumpForward
" Escape command mode
cnoremap kj <C-c> 
" Escape visual mode
vnoremap kj <Esc>
" Quick escape
inoremap kj <Esc>
"Re-indent file 
map <leader>ind mzgg=G`z
" Treat long lines as break lines (useful when moving around in them)
map j gj
map k gk
" Disable highlight when <leader><cr> is pressed
map <silent> <leader><cr> :noh<cr>
" Switch CWD to the directory of the open buffer
map <leader>cd :cd %:p:h<cr>:pwd<cr>
command W w !sudo tee % > /dev/null
" Disable auto-comment on new line
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" EasyMotion 
" Move to line
map <Leader>l <Plug>(easymotion-bd-jk)
nmap <Leader>l <Plug>(easymotion-overwin-line)
" Move to word
map  <Leader>s <Plug>(easymotion-bd-w)
nmap <Leader>s <Plug>(easymotion-overwin-w)
